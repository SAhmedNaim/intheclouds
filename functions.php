<?php

add_theme_support( 'title-tag' );

$default_header_image = array(
	'default-image'	=> get_template_directory_uri().'/images/picture.jpg'
);
add_theme_support( 'custom-header', $default_header_image );

$default_background_image = array(
	'default-image' => get_template_directory_uri().'/images/background.png'
);
add_theme_support( 'custom-background', $default_background_image );

function left_sidebar()
{
	register_sidebar(array(
		'name'			=> 'Left Sidebar',
		'description'	=> 'You can add left sidebar widgets here...',
		'id'			=> 'leftsidebar',
		'before_title'	=> '<h2>',
		'after_title'	=> '</h2>',
		'before_widget'	=> '',
		'after_widget'	=> ''
	));

	register_sidebar(array(
		'name'			=> 'Right Sidebar',
		'description'	=> 'You can add right sidebar widget here...',
		'id'			=> 'rightsidebar',
		'before_title'	=> '<h2>',
		'after_title'	=> '</h2>',
		'before_widget'	=> '',
		'after_widget'	=> ''
	));

}
add_action( 'widgets_init', 'left_sidebar' );

add_theme_support( 'post-thumbnails' );
