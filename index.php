<!DOCTYPE html>
<head>
    <meta charset="utf-8" />
    <link rel="stylesheet" type="text/css" href="<?php bloginfo('stylesheet_url'); ?>" />
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<div id="container">
	<div id="header">
	    <div id="picture" style="background: url(<?php header_image(); ?>) no-repeat 0 0 / 100% 100%;">
            <h1><?php bloginfo( 'name' ); ?></h1>
            <h2><?php bloginfo( 'description' ); ?></h2>
        </div>
    </div>
    <div id="main">
    <div id="leftcol_container">
    	<div class="leftcol">

        	<?php dynamic_sidebar( 'leftsidebar' ); ?>

        	<p>&nbsp;</p>

        	
            <a href="#">Contact Us</a>
      </div>
      <div class="leftcol_bottom"></div>
      </div>
      
        <div id="maincol_container">
            <div class="maincol">   
            
                <?php while (have_posts()) : the_post(); ?>

                    <h2><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
                    <?php the_post_thumbnail('medium'); ?>
                    <p><?php the_content(); ?></p>
            
                <?php endwhile; ?>
            
            <p>&nbsp;</p>
            
            
            </div>
        
        </div>
        
        
     <div id="rightcol_container">
        
        
        <div class="rightcol">
            
            <?php dynamic_sidebar( 'rightsidebar' ); ?>

        </div>
        <div class="rightcol_bottom"></div>
        
        <div class="clear"></div>
        
         <div id="footer"><a href="http://www.bryantsmith.com/template">free template css</a> by <a href="http://www.bryantsmith.com/">web design florida</a></div>
  </div>

</div>
    

    <?php wp_footer(); ?>
</body>
</html>
